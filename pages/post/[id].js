import Layout from "../../components/layout";

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import Grid from '@material-ui/core/Grid';
import Skeleton from '@material-ui/lab/Skeleton';

import React from 'react';
import fetch from 'isomorphic-unfetch';
import Cookies from 'js-cookie'

import Router from 'next/router'
import { auth, withAuthSync } from '../../utils/auth'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

import * as URL from '../../utils/urls'
import MyEditor from '../MyEditor'
import MessageBar from '../messagebar'
import IndeterminateProgress from '../indeterminateprogress';



const styles = theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: "100%",

        },
    },
    avatar: {
        display: 'inline-block',
        '& .MuiAvatar-root': {
            height: 60,
            width: 60,
            marginRight: 24
        }
    },
    username: {
        display: 'inline-block',
        fontSize: 14,
        fontWeight: 100
    },
    commentText: {

        fontSize: 20

    },
    rightOfAvatar: {
        display: 'inline-block'
    },

    title: {
        color: '#A10020',
        fontSize: 49,
        textAlign: "center",
        fontWeight: 500,
    },
    category: {
        textAlign: "center",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "lightgrey",
        marginTop: 13,
        marginBotton: 13,
        color: "grey",
        fontWeight: 600,
        fontSize: 22,
    },
    detail: {
        fontSize: 22,
    },
    editor: {
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: '#ff1744'
    },
    editorValidationText: {
        color: '#ff1744',
        marginLeft: 15,
        marginTop: 5
    },
    button: {
        marginTop: 40,
        display: 'block',
        width: '100%',
        margin: 17,
        borderRadius: 0,
        backgroundColor: 'white',
        height: 54,
        borderWidth: 1,
        borderColor: 'grey',
        borderStyle: 'solid',
        color: 'grey',
    },
    formWrapper: {
        '& .MuiButton-root:hover': {
            backgroundColor: '#A10020',
            color: 'white',
            borderColor: 'white',
        },
        '& label.Mui-focused': {
            color: '#A10020',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: '#A10020',
        },
        '& .MuiOutlinedInput-root': {
            borderRadius: 0,
            '&.Mui-focused fieldset': {
                borderColor: '#A10020',
            },
        }
    }

});

class Post extends React.Component {

    constructor(props) {
        super(props)
        this._ref = props._ref
        this.state = {
            comments: this.props.comments,
            comment: '',
            commentIsInValid: false,
            isLoading: false,
            error_messages: [],
            info_messages: [],
            success_messages: [],
            loadingComments: false,

        }

    }

    updateComments = async () => {
        this.setState({ loadingComments: true })
        const res_comments = await fetch(URL.GET_COMMENTS_OF_POST + '/' + this.props.post[0]._id)
        const comment_details = await res_comments.json();
        this.setState({
            comments: comment_details,
            loadingComments: false
        })
    }


    handleEditorChange = (content, editor) => {
        if (this.isValid(content)) {
            this.setState({ commentIsInvalid: false })
            this.setState({ error_messages: [] })
        }
        this.setState({ comment: content })
    }


    isValid = (text) => {
        const cond = text.length > 2
        return cond
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state.comment)


        const myData = new FormData(e.target)
        console.log('///////////')
        console.log('///////////')
        console.log('///////////')
        console.log('///////////')
        console.log(this.props)

        if (!this.isValid(this.state.comment)) {
            console.log('yo no tengo')
            this.setState({ commentIsInvalid: true })
        }
        else {
            this.setState({ commentIsInValid: false })
            this.setState({ isLoading: true })
            this.setState({ info_messages: ["Posting Comment ..."] })

            fetch(URL.ADD_COMMENT,
                {
                    method: 'post',
                    body: myData,
                    headers: {
                        Authorization: 'Bearer ' + Cookies.get('access_token'),
                        'Access-Control-Allow_Origin': '*'
                    }
                }).then(res => {
                    return Promise.all([res.json(), res.status])
                }).then(result => {
                    const data = result[0]
                    const resCode = result[1]

                    console.log('Response Code: ' + resCode + " Data: " + data['error'])
                    if (resCode === 401 || resCode === 422) {
                        Router.push('/login')
                    } else if (resCode === 400) {
                        //this.handleErrorFromServer(data['error'])
                        console.log(data)
                    } else if (resCode === 200) {

                        this.updateComments()
                        this.setState({ comment: '' })
                        this.setState({ isLoading: false })
                        this.setState({ info_messages: [] })
                        this.setState({ success_messages: ['Successfully Posted Comment!'] })
                        setTimeout(() => this.setState({ success_messages: [] }), 2000)


                    } else {
                        Router.push('/login')
                    }
                })
        }

    }

    render() {


        const { classes } = this.props;



        const post = this.props.post[0]



        const Comments = () => this.state.comments.map(comment => {
            return (

                <div className={classes.comment}>
                    <ListItemAvatar className={classes.avatar}>
                        <Avatar alt={comment.user} />
                    </ListItemAvatar>
                    <div className={classes.rightOfAvatar}>
                        <div className={classes.username}>{comment.user}</div>
                        <div className={classes.commentText}
                            dangerouslySetInnerHTML={{ __html: comment.comment }} />
                    </div>
                   
                   
                </div>

            )
        })


        return (
            <Layout isLoggedIn={this.props.isLoggedIn}>
                <MessageBar
                    errorMessages={this.state.error_messages}
                    infoMessages={this.state.info_messages}
                    successMessages={this.state.success_messages}
                />

                <Grid container>
                    <Grid item xs={3}></Grid>

                    <Grid item xs={6}>
                        <div>

                            <div className={classes.title}>
                                {post.title}
                            </div>
                            <div className={classes.category}>
                                {post.category_name}
                            </div>
                            <div className={classes.detail} dangerouslySetInnerHTML={{ __html: post.detail }} />
                            {this.props.isLoggedIn ?
                                <div>

                                    <form className={classes.root} onSubmit={this.handleSubmit}>
                                        <input name="comment" className={classes.textField} value={this.state.comment}
                                            onChange={this.handleDetailChange} type="hidden"
                                        />
                                        <div className={this.state.commentIsInvalid ? classes.editor : null}>

                                            <MyEditor
                                                onEditorChange={this.handleEditorChange}
                                                value={this.state.comment}
                                                disabled={this.state.isLoading} />

                                        </div>

                                        {this.state.commentIsInvalid ? <p className={classes.editorValidationText}>Comment must contain at least one character</p> : null}
                                        {this.state.isLoading ? <IndeterminateProgress /> : null}
                                        <input
                                            type="hidden"
                                            value={post._id}
                                            name="post_id" />
                                        <div className={classes.formWrapper}>
                                            <Button
                                                className={classes.button}
                                                type="submit"
                                                disabled={this.state.isLoading}
                                            >
                                                Comment As {this.props.profile.email}
                                            </Button>
                                        </div>

                                    </form>
                                </div>
                                :
                                <div className={classes.formWrapper}>
                                    <Button
                                        className={classes.button}
                                        onClick={() => Router.push('/login')}
                                        disabled={this.state.isLoading}
                                    >
                                        Log In To Comment
                                </Button>
                                </div>
                            }
                            <List className={classes.root}>
                                {this.state.loadingComments ? <Skeleton animation="wave"></Skeleton> : null}
                                <Comments />

                            </List>
                        </div>
                    </Grid>
                    <Grid item xs={3}></Grid>
                </Grid>
            </Layout>
        )
    }
}

Post.getInitialProps = async function (context) {



    const res_post = await fetch(URL.GET_POST + '/' + context.query.id);

    const post_details = await res_post.json();


    const res_comments = await fetch(URL.GET_COMMENTS_OF_POST + '/' + context.query.id)
    const comment_details = await res_comments.json();
    console.log("--------------")

    console.log(comment_details)
    console.log("--------------")

    const res_profile = await fetch(URL.GET_PROFILE, {
        headers: {
            Authorization: 'Bearer ' + auth(context),
            'Access-Control-Allow_Origin': '*'
        }
    })



    const data = await res_profile.json()


    // console.log("data" + data)


    return {
        //        id: context.query.id,
        post: post_details,
        comments: comment_details,
        profile: data

        // title: data.post.title,
        // detail: data.post.detail
    }
}

export default withAuthSync(withStyles(styles)(Post));