import React from 'react'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import { withStyles } from '@material-ui/core/styles';
import Layout from "../../components/layout";
import { auth, withAuthSync } from '../../utils/auth';
import Router from 'next/router'
import ErrorIcon from '@material-ui/icons/Error';
import Cookies from 'js-cookie'
import fetch from 'isomorphic-unfetch';
import * as URL from '../../utils/urls';
import IndeterminateProgress from '../indeterminateprogress';

import MessageBar from '../messagebar'

import { FormHelperText } from '@material-ui/core'
import MyEditor from '../MyEditor'





const styles = theme => ({
    root: {
        '& > *': {
            marginTop: 10,
        },
    },
    errorDisplay: {
        color: 'red',
        '& p': {
            display: 'inline-block',
            marginLeft: 13,
            textAlign: 'center',
            verticalAlign: 'super'
        }
    },
    textField: {
        width: '100%',
        color: '#F90'
    },

    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    title: {
        textAlign: 'center',
        color: '#A10020',
        fontSize: 49,
        fontWeight: 500
    },
    button: {
        marginTop: 40,
        display: 'block',
        width: '100%',
        margin: 17,
        borderRadius: 0,
        backgroundColor: 'white',
        height: 54,
        borderWidth: 1,
        borderColor: 'grey',
        borderStyle: 'solid',
        color: 'grey',
    },
    editor: {
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: '#ff1744'
    },
    editorValidationText: {
        color: '#ff1744',
        marginLeft: 15,
        marginTop: 5
    },
    formWrapper: {
        '& .MuiButton-root:hover': {
            backgroundColor: '#A10020',
            color: 'white',
            borderColor: 'white',
        },
        '& label.Mui-focused': {
            color: '#A10020',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: '#A10020',
        },
        '& .MuiOutlinedInput-root': {
            borderRadius: 0,
            '&.Mui-focused fieldset': {
                borderColor: '#A10020',
            },
        }
    }
});

class NewPost extends React.Component {
    constructor(props) {
        super(props)
        this._ref = props._ref

        this.state = {
            title: '',
            errorMessages: [],
            category: '',
            detail: '',
            categories: props.categories,
            titleFieldIsInvalid: false,
            detailFieldIsInvalid: false,
            categoryFieldIsInvalid: false,
            isLoading: false,
            error_messages: [],
            info_messages: [],
            success_messages: [],


        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleTitleChange = this.handleTitleChange.bind(this)
        
        this.displayErrorMessages = this.displayErrorMessages.bind(this)
        this.handleErrorFromServer = this.handleErrorFromServer.bind(this)
    }

    componentDidMount() {
        this.authCheck(this.props.isLoggedIn)
    }

    authCheck = (isLoggedIn) => {

        if (!isLoggedIn) {
            Router.push('/login')
        }
        return ''
    }

    validateData() {
        var retValue = []
        if (this.state.title.length < 1) {
            retValue.push('Title must contain at least one character')
        }
        if (this.state.detail.length < 1) {
            retValue.push('Detail must contain at least one character')
        }
        if (!this.state.categories.includes(this.state.category))
            console.log('i am blinded by the lights')
            retValue.push('Category selected is not valid')
        return retValue
    }

    handleSubmit(e) {
        e.preventDefault()
        
       

        this.setState({ errorMessages: [] })
        //var validatedMessages = this.validateData()
        //this.setState({ errorMessages: validatedMessages })
        

        //if (validatedMessages.length === 0) {



        if (!this.isValid(this.state.title) || !this.isValid(this.state.detail) || !this.isValid(this.state.category)) {
        //if (!this.isValid(this.state.title) || !this.isValid(this.state.category)) {
            console.log('que pasa')
            console.log(this.state.category)
            if (!this.isValid(this.state.title)) {
                this.setState({ titleFieldIsInvalid: true })
            }
            if (!this.isValid(this.state.detail)) {
                console.log(this.state.detail)
                this.setState({ detailFieldIsInvalid: true })
            }
            if (!this.isValid(this.state.category)) {
                this.setState({ categoryFieldIsInvalid: true })
            }
        }
        else {
            console.log('isnt it loading?')
            this.setState({ isLoading: true })
            this.setState({ info_messages: ['Posting ...'] })


            const myData = new FormData(e.target)

            fetch(URL.ADD_POST,
                {
                    method: 'post',
                    body: myData,
                    headers: {
                        Authorization: 'Bearer ' + Cookies.get('access_token'),
                        'Access-Control-Allow_Origin': '*'
                    }
                }).then(res => {
                    return Promise.all([res.json(), res.status])
                }).then(result => {
                    this.setState({ isLoading: false })
                    this.setState({ info_messages: [] })
                    const data = result[0]
                    const responseCode = result[1]
                    console.log(data)
                    console.log(responseCode)
                    if (responseCode == 200) {
                        Router.push('/')
                    }
                    else if (responseCode == 400) {
                        console.log('hiiiiii')
                        this.handleErrorFromServer(data['error'])
                    }
                    else if (responseCode === 401 || responseCode == 422){
                        Router.push('/login')
                        
                    }
                    else {
                        console.log('is this data')
                        throw data
                    }

                })
                .catch(e => {
                    console.log(e)
                    this.setState({ info_messages: [] })
                    if (responseCode !== 400) {
                        this.setState({ error_messages: [e.msg] })
                    }
                })

            /*
            console.log('Response Code: ' + resCode + " Data: " + data['error'])
            if (resCode === 401 || resCode === 422) {
                Router.push('/')
            } else if (resCode === 400) {
                this.handleErrorFromServer(data['error'])
                this.setState({ isLoading: false })
                this.setState({ info_messages: [] })
                this.setState({ erro_messages: [data['error']]})
            } else if (resCode === 200) {
                Router.push('/')
            } else {
                Router.push('/login')
            }
        })
            */

        }
    }

    handleErrorFromServer(errors) {
        
        const eMessages = []
        for (var key in errors) {
            for (var i = 0; i < errors[key].length; i++) {
                const error = errors[key][i]
                eMessages.push(key + error)
            }
        }

        this.setState({ errorMessages: eMessages })
    }

    playAnimation() {
        document.getElementById('myCollaspe').in = true
    }
    handleTitleChange(e) {
        if (this.isValid(e.target.value)) {
            this.setState({ titleFieldIsInvalid: false })
            this.setState({ error_messages: [] })
        }
        this.setState({ title: e.target.value })
    }

    

    handleEditorChange = (content, editor) => {
        console.log(content)
        if (this.isValid(content)) {
            this.setState({ detailFieldIsInvalid: false })
            this.setState({ error_messages: [] })
        }
        this.setState({ detail: content })
    }

    handleCategoryChange = (e) => {
        console.log(e.target.value);
        this.setState({ category: e.target.value })
        if (this.isValid(e.target.value)) {
            this.setState({ categoryFieldIsInvalid: false })
            this.setState({ error_messages: [] })
        }

    }


    isValid = (text) => {
        const cond = text.length > 2
        return cond
    }


    displayErrorMessages = () => {
        const items = []

        if (this.state.errorMessages.length === 0) {
            return null
        }

        for (var i = 0; i < this.state.errorMessages.length; i++) {
            items.push(
                <div keys={'error_key_' + i}>
                    <ErrorIcon /> <p>{this.state.errorMessages[i]}</p>
                </div>
            )
        }

        return (
            
                <div>{items}</div>
            
        )
    }

    render() {

        const { classes } = this.props;

        const MenuItems = this.state.categories.map(x => {
            return (
                <MenuItem value={x._id}> {x.category_name}</MenuItem>
            )
        })

        const middleFullWidth = 12;
        const middleSmall = 10;


        const side = 1;


        return (

            <Layout isLoggedIn={this.props.isLoggedIn}>
                <MessageBar
                    errorMessages={this.state.error_messages}
                    infoMessages={this.state.info_messages}
                    successMessages={this.state.success_messages}
                />
                <div class={classes.errorDisplay}>
                    <this.displayErrorMessages />
                </div>



                <form onSubmit={this.handleSubmit} className={classes.formWrapper}>



                    <Grid container alignContent="center" className={classes.root}>

                        <Grid xs={middleFullWidth} item align="center">
                            <p className={classes.title}>New Post</p>
                        </Grid>
                        <Grid md={side} item />
                        <Grid xs={middleFullWidth} md={middleSmall} item>
                            <TextField name="title"
                                error={this.state.titleFieldIsInvalid || (this.state.error_messages.length !== 0)}
                                label="Title"
                                id={this.state.titleFieldIsInvalid === true ? "outlined-error-helper-text" : 'outlined-basic'}
                                variant="outlined"
                                value = {this.state.title}
                                className={classes.textField}
                                onChange={this.handleTitleChange}
                                helperText={this.state.titleFieldIsInvalid === true ? 'Title must contain at least one character' : null}
                                disabled={this.state.isLoading}
                            />
                        </Grid>
                        <Grid md={side} item />
                    </Grid>


                    <Grid container alignContent="center" className={classes.root}>
                        <Grid md={side} item />
                        <Grid xs={middleFullWidth} md={middleSmall} item>
                            <input name="detail" className={classes.textField} value={this.state.detail}
                                onChange={this.handleEditorChange} type="hidden"
                            />
                            <div className={this.state.detailFieldIsInvalid ? classes.editor : null}>
                                <MyEditor onEditorChange={this.handleEditorChange} value={this.state.detail} disabled={this.state.isLoading}/>
                                
                            </div>
                            {this.state.detailFieldIsInvalid ? <p className={classes.editorValidationText}>Detail must contain at least one character</p> : null}
                            
                        </Grid>
                        <Grid md={side} item />
                    </Grid>


                    <Grid container alignContent="center" className={classes.root}>
                        <Grid md={side} item />
                        <Grid xs={middleFullWidth} md={middleSmall} item>
                            <FormControl variant="filled" className={classes.formControl} error={this.state.categoryFieldIsInvalid}>

                                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                                <Select
                                    error={this.state.categoryFieldIsInvalid}
                                    helperText={this.state.categoryFieldIsInvalid === true ? 'hi' : null}
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.category}
                                    onChange={this.handleCategoryChange}
                                    className={classes.selectEmpty}
                                    variant="outlined"
                                    labelWidth={65}
                                    name="category"
                                >

                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>

                                    {MenuItems}
                                </Select>
                                {this.state.categoryFieldIsInvalid ? <FormHelperText>Please select a category</FormHelperText> : null}

                            </FormControl>


                            <Button type="submit" className={classes.button} disabled={this.state.isLoading}>
                                Create Post
                            </Button>
                            {this.state.isLoading ? <IndeterminateProgress /> : null}
                            
                            


                        </Grid>
                        <Grid md={side} item />

                    </Grid>

                </form>
            </Layout>
        )
    }
}


NewPost.getInitialProps = async function (context) {
    const res = await fetch(URL.GET_ALL_CATEGORIES);
    const data = await res.json();

    return { categories: data };
}


export default withAuthSync(withStyles(styles)(NewPost));



/*
<TextField name="detail" multiline rows={10} label="Detail" id="outlined-basic" variant="outlined" className={classes.textField} value={this.state.detail}
                                onChange={this.handleDetailChange} type="hidden"
                            />
                            */

/*
                         <Editor
                                    disabled={this.state.isLoading}
                                    iditialValue="<p>this is the initial content of the editor </p>"
                                    value = {this.state.detail}
                                    apiKey="f6of9mnronvokajb00k69wag6uutos5ng7m75gewlajhlh1w"
                                    init={{
                                        height: 500,
                                        menubar: false,
                                        selector: 'textarea',
                                        images_upload_handler: function (blobInfo, success, failure) {
                                            setTimeout(function () {
                                                var form = new FormData();
                                                form.append('File', blobInfo.blob(), blobInfo.blob().name);
                                                let data = new FormData();
                                                data.append('name', 'image')
                                                data.append('file', blobInfo.blob())
                                                let config = {
                                                    header: {
                                                        'Content-Type': 'multipart/form-data'
                                                    }
                                                }
                                                axios.post(URL.UPLOAD_PHOTO, data, config).then(response => {
                                                    console.log('response', response)
                                                    success(response.data)

                                                }).catch(error => {
                                                    console.log('error', error)
                                                })
                                            });

                                        },
                                        plugins: [
                                            'advlist autolink lists link image imagetools charmap print preview anchor',
                                            'searchreplace visualblocks code fullscreen',
                                            'insertdatetime media table paste code help wordcount'
                                        ],
                                        toolbar:
                                            'undo redo | formatselect | bold italic backcolor | \
                                        alignleft aligncenter alignright alignjustify | \
                                        bullist numlist outdent indent | removeformat | image code\
                                        '
                                    }}
                                    onEditorChange={this.handleEditorChange}  
                                />
                                */