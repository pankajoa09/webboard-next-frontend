import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import { logout } from '../../../utils/auth'

import Layout from '../../../components/layout';
import Router from 'next/router'
import { useRouter } from 'next/router'
//import {customRedirect} from '../utils/basic_utils'
import { withAuthSync } from '../../../utils/auth'
import { withStyles } from '@material-ui/core/styles';
import fetch from 'isomorphic-unfetch'

import Cookies from 'js-cookie'

import * as URL from '../../../utils/urls';
import MessageBar from '../../messagebar'
import IndeterminateProgress from '../../indeterminateprogress'

const styles = theme => ({
    logout: {
        textAlign: "center"
    },
    title: {
        color: '#A10020',
        fontSize: 49,
        textAlign: "center",
        fontWeight: 500,
    },
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },

});


class DeletePost extends React.Component {

    constructor(props) {
        super(props)
        this._ref = props._ref
        //this.authCheck = this.authCheck.bind(this);
        console.log(props)
        this.state = {
            isLoading: false,
            error_messages: [],
            success_messages: [],
            info_messages: []

        }
    }

    componentDidMount() {
        this.authCheck(this.props.isLoggedIn);
    }

    authCheck = (isLoggedIn) => {
        console.log(isLoggedIn, 'is he?')
        if (!isLoggedIn) {
            Router.push('/login')
        }
        return '';
    }

    handleDeletePost = async () => {
        console.log(this.props.post[0]._id)
    
        this.setState({isLoading:true})
        this.setState({info_messages: ['Deleting Post ...']})
        await fetch(URL.DELETE_POST + '/' + this.props.post[0]._id, {
            method: 'post',
            headers: {
                Authorization: 'Bearer ' + Cookies.get('access_token'),
                'Access-Control-Allow_Origin': '*'
            }
        }).then(res => {
            return Promise.all([res.status, res.json()])
        })
            .then(result => {
                this.setState({ isLoading: false })
                const responseCode = result[0]
                const data = result[1]

                if (responseCode === 200) {
                    console.log("sfjdkljkldfslkjfkjllkjsdfjlkdfjkkjdklf")
                    this.setState({success_messages: ["Post deleted!"]})
                    setTimeout(() => this.setState({ success_messages: [] }), 1000)
                    Router.push('/profile')
                    
                } 
                else if (responseCode === 401 || responseCode == 422){
                    Router.push('/login')
                    
                }
                else {
                    console.log('is this data')
                    throw data
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({ error_messages: [e.message] })




            })

    }

    handleCancel = () => {
        Router.push('/profile');
    }


    render() {
        const { classes } = this.props;

        return (

            <Layout isLoggedIn={this.props.isLoggedIn}>
                <MessageBar
                    errorMessages={this.state.error_messages}
                    infoMessages={this.state.info_messages}
                    successMessages={this.state.success_messages}
                />
                <div className={classes.logout}>
                    <p className={classes.title}>Delete Post </p>
                    <h1>Are you sure you want to delete your post "{this.props.post[0].title}"? </h1>
                    <h1>You can't undo this.</h1>
                    <Button onClick={this.handleDeletePost}>Yes</Button>
                    <Button onClick={this.handleCancel}>No</Button>
                    {this.state.isLoading ? <IndeterminateProgress /> : null}
                </div>
                
            </Layout>

        )
    }
}

DeletePost.getInitialProps = async function (context) {
    console.log("whattt")
    console.log(context.query.delete_id)
    const res_post = await fetch(URL.GET_POST + '/' + context.query.delete_id);
    const post_details = await res_post.json();


    return { post: post_details }
    //return{}
}


export default withAuthSync(withStyles(styles)(DeletePost));