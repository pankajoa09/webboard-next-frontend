import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { login, auth } from '../utils/auth'
import Layout from '../components/layout';
import Router from 'next/router';
import { useRouter } from 'next/router';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';


import styled from 'styled-components';

//import {customRedirect} from '../utils/basic_utils'
import { withAuthSync } from '../utils/auth'

import * as URL from '../utils/urls';

import Interactive from './interactive'
//import ProgressBar from './progressbar'
import LinearProgress from '@material-ui/core/LinearProgress';
import IndeterminateProgress from './indeterminateprogress';





function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}






const styles = theme => ({

});



class MessageBar extends React.Component {

    constructor(props) {
        super(props)
        this._ref = props._ref
        const {errorMessages,successMessages,infoMessages} = this.props
        
        this.state = {
            error_messages: errorMessages === undefined ? [] : props.errorMessages,
            success_messages: successMessages === undefined ? [] : props.successMessages,
            info_messages: infoMessages === undefined ? [] : props.infoMessages,

        }
        //this.authCheck = this.authCheck.bind(this);
        
    }

    


    render() {
        const { classes } = this.props;
        
        const {errorMessages,successMessages, infoMessages } = this.props

        
        return (
            <div>
                <Snackbar open={errorMessages.length !== 0} autoHideDuration={6000} >
                    <Alert severity="error">{errorMessages[0]}</Alert>
                </Snackbar>
                <Snackbar open={successMessages.length !== 0} autoHideDuration={6000} >
                    <Alert severity="success">{successMessages[0]}</Alert>
                </Snackbar>
                
                <Snackbar open={infoMessages.length !== 0} autoHideDuration={6000} >
                    <Alert severity="info">{infoMessages[0]}</Alert>
                </Snackbar>
            </div>
        )
    }
}





export default withStyles(styles)(MessageBar);