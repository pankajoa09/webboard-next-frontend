import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import {logout} from '../utils/auth'
import Layout from '../components/layout';
import Router from 'next/router'
import {useRouter} from 'next/router'
//import {customRedirect} from '../utils/basic_utils'
import {withAuthSync} from '../utils/auth'
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    logout: {
        textAlign: "center"
    },
    
    
});


class Logout extends React.Component{

    constructor(props){
        super(props)
        this._ref = props._ref
        
        
        
        
        
        
        //this.authCheck = this.authCheck.bind(this);
        console.log(props)
        
    }

    componentDidMount(){
        this.authCheck(this.props.isLoggedIn);
    }

    authCheck = (isLoggedIn) => {
        console.log(isLoggedIn,'is he?')
        if (!isLoggedIn){
            Router.push('/login')
        }
        return '';
    }

    handleLogout = () => {
        logout();
    }

    handleStayLoggedIn = () => {
        Router.push('/profile');
    }


    render(){
        const {classes} = this.props;
        
        return (

            <Layout isLoggedIn={this.props.isLoggedIn}>
            <div className={classes.logout}>
            <h1>Sure?</h1>
            <Button onClick={this.handleLogout}>Yes</Button>
            <Button onClick={this.handleStayLoggedIn}>No</Button>
            
            </div>
            </Layout>
            
        )
    }
}



export default withAuthSync(withStyles(styles)(Logout));