import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: theme.spacing.unit, // You might not need this now
        
        bottom: theme.spacing.unit * 0,
        right: theme.spacing.unit * -1,
        height: 1,
        '& > * + *': {
            marginTop: theme.spacing(2),

        },
        '& .MuiLinearProgress-colorPrimary': {
            backgroundColor: '#A100308f',
            height: 11,
            '& .MuiLinearProgress-barColorPrimary': {
                backgroundColor: '#A10030'
            }
        },
    }
}));

export default function ProgressBar(props) {
    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);

    console.log(props.percentage)

    
    React.useEffect(() => {
        function progress() {
            setCompleted((oldCompleted) => {
                if (oldCompleted === 100) {
                    return 0;
                }
                const diff = Math.random() * 10;
                return Math.min(oldCompleted + diff, 100);
            });
        }

        const timer = setInterval(progress, 500);
        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <div className={classes.root}>

            <LinearProgress variant="determinate" value={completed} className={classes.smth} />
        </div>
    );
}