import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: theme.spacing.unit, // You might not need this now
        
        bottom: theme.spacing.unit * 0,
        right: theme.spacing.unit * -1,
        height: 1,
        '& > * + *': {
            marginTop: theme.spacing(2),

        },
        '& .MuiLinearProgress-colorPrimary': {
            backgroundColor: '#A100308f',
            height: 11,
            '& .MuiLinearProgress-barColorPrimary': {
                backgroundColor: '#A10030'
            }
        },
    }
}));


export default function IndeterminateProgress() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <LinearProgress />
      
    </div>
  );
}