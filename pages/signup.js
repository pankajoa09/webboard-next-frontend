import React from 'react'
import Cookies from 'js-cookie'
import Layout from '../components/layout';
import { login } from '../utils/auth'
import Router from 'next/router'
import * as URL from '../utils/urls';

const errorMessageMapping = {
    1: 'Email must conatain @',
    2: 'Password And Password Confirmation must be the same'
}


class SignupForm extends React.Component{
    constructor(props){
        super(props)
        this._ref = props._ref
        var errorMessages = {}
        for (var key in errorMessages){
            errorMessages[key] = false
        }

        this.state={
            email: '',
            password: '',
            confirmPassword: '',
            errorMessages: errorMessages
        }
        this.handleSubmit = this.handleSubmit.bind(this)

        this.handleEmailChange = this.handleEmailChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleConfirmPasswordChange = this.handleConfirmPasswordChange.bind(this)
        this.displayErrorMessages = this.displayErrorMessages.bind(this)

    }


    componentDidMount(){
        this.authCheck(this.props.isLoggedIn)
    }

    authCheck = (isLoggedIn) => {
        if (isLoggedIn){
            Router.push('/profile')
        }
        return ''
    }

    handleSubmit(e){
        e.preventDefault();
        var myData = new FormData(e.target)
        fetch(URL.SIGNUP, {
            method: 'post',
            body:myData
        }).then(res =>{
            return Promise.all([res.json(), res.status])
        }).then(result => {
            const resCode = result[1]
            const data = result[0]
            if (resCode === 400){
                var errors = result[0].error;

                for (const key in errors){
                    for(var i=0;i<errors[key].length;i++){
                        console.log(key, 'Error ----- ',errors[key][i]);
                    }
                }
                
            } else if (resCode === 200){
                const {access_token} = data;
                login(access_token)
                Router.push('/profile')
            }
        })
    }

    handlePasswordChange(e){
        console.log("password: " + e.target.value)
        var errorDict = this.state.errorMessages;

        this.setState({password:e.target.value})
        if (e.target.value !== this.state.confirmPassword && this.state.confirmPassword.length > 0){
            errorDict[2] = true
        } else {
            errorDict[2] = false
        }
    }

    handleConfirmPasswordChange(e){
        console.log("confirm_password: " + e.target.value)
        var errorDict = this.state.errorMessages;


        this.setState({confirmPassword:e.target.value})
        if (e.target.value !== this.state.password && this.state.password.length > 0){
            // console.log(this.state.password)
            errorDict[2] = true
        } else {
            errorDict[2] = false
        }
    }

    handleEmailChange(e){
        console.log("email: " + e.target.value)
        var errorDict = this.state.errorMessages;

        this.setState({email:e.target.value})
        if (!e.target.value.includes('@')){
            e.target.style.color='#F00'
            errorDict[1] = true
        } else {
            e.target.style.color='#000'
            errorDict[1] = false
        }
    }

    displayErrorMessages(){
        if (this.state.errorMessages.length === 0){
            return ''
        }

        const items = []
        for(var key in this.state.errorMessages){
            var message = this.state.errorMessages[key]

            if (message === true){
                items.push(<li key={key}>{errorMessageMapping[key]}</li>)
            }
        }

        return <ul>{items}</ul>
    }



    render(){
        return (
            <Layout isLoggedIn={this.props.isLoggedIn}>
                <div>
                    <h1>Sign Up</h1>
                    <this.displayErrorMessages />
                    <form action="/login" onSubmit={this.handleSubmit}>
                        <input name="email" placeholder="Email" onChange={this.handleEmailChange} />
                        <br />
                        <input name="password" placeholder="Password" type="password" onChange={this.handlePasswordChange} />
                        <br />
                        <input name="confirmPassword" placeholder="Confirm Password" type="password" onChange={this.handleConfirmPasswordChange} />
                        <br />
                        <input value="Sign Up" type="submit" />
                    </form>
                </div>
            </Layout>
        )
    }
}



export default SignupForm;