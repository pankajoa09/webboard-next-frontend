import React from 'react'
import Cookies from 'js-cookie'
import Layout from '../components/layout';
import fetch from 'isomorphic-unfetch';
import { auth, withAuthSync } from '../utils/auth'
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import AssignmentIcon from '@material-ui/icons/Assignment';


import MessageBar from './messagebar'


import { customRedirect } from '../utils/basic_utils'
import Router from 'next/router';
import Link from '../src/Link'

import * as URL from '../utils/urls';





const styles = theme => ({
    root: {

    },
    user: {
        paddingBottom: 30
    },
    post: {
        borderStyle: 'solid',
        padding: 0,
        borderRadius: 0,
        borderWidth: 1,
        paddingLeft: 24,
        paddingRight: 24,
        borderColor: 'lightgrey',


    },
    post_title: {
        fontSize: 29,
        paddingLeft: 21,
        color: 'black',

    },
    post_action: {
        paddingLeft: 21,
        '& a': {
            display: 'inline-block',
            paddingRight: 5,
            fontSize: 17

        }

    },
    post_details: {
        display: 'inline-block'
    },
    post_image: {
        height: 100,
        objectFit: "cover",
        width: 100,
        borderRadius: 10,
        display: 'inline-block'

    },
    postlist: {

    },
    no_posts: {
        textAlign: 'center',
        borderColor: 'lightgrey',
        borderStyle: 'solid',
        verticalAlign: 'middle',
        height: 131,
        borderWidth: 2,
        borderRadius: 14

    }
});

class Profile extends React.Component {

    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            isLoading: false,
            authState: true,
            _id: props.profile.data._id,
            email: props.profile.data.email,
            posts_data: props.profile.posts_data,
            error_messages: [],
            success_messages: [],
            info_messages: []
        }
        this.displayUserDetail = this.displayUserDetail.bind(this)
        console.log('////////////////')

        console.log(props)
        console.log('////////////////')

        this.props = props

    }

  

    componentDidMount() {
        console.log(this.state)
        this.authCheck(this.props.isLoggedIn)
        this.justLoggedIn(this.props.isLoggedIn, this.props.history)

        
        
        


    }

    justLoggedIn = (isLoggedIn, history) => {

        const wasLoggingIn = history[history.length-1] === '/login'
        console.log(history)
        console.log(wasLoggingIn)
        console.log(isLoggedIn)
        if (wasLoggingIn && isLoggedIn) {
            console.log('ye')
            this.setState({success_messages: ['Welcome, '+ this.state.email]})
            setTimeout(() => this.setState({ success_messages: [] }), 3000)
            
            
        }
        console.log('nay')
    }

    authCheck = (isLoggedIn) => {
        if (!isLoggedIn) {
            Router.push('/login')
        }
        return ''
    }


    displayUserDetail() {
        if (this.state.isLoading) {
            return <div>is loading..</div>
        } else {
            if (this.state.authState) {
                return (
                    <div>
                        <h1>Welcome {this.state.email}</h1>
                        <ul>
                            {this.state.posts_data.map(x => <div>{x.title}</div>)}
                        </ul>
                    </div>
                )

            } else {
                return <div>Not Authorized</div>
            }
        }
    }

    displayUserPosts = () => {
        console.log(this.state.posts_data)
    }
    render() {

        const { classes } = this.props;
        
        console.log(this.state.posts_data)
        const Posts = () => {
            console.log('k')
            if (this.state.posts_data.length !== 0) {
                console.log('watt')
                return (
                    this.state.posts_data.map(x => {
                        return (
                            <div className={classes.post}>

                                <Avatar variant="rounded" className={classes.post_image}>
                                    {x.thumbnail_url === '' ? <AssignmentIcon className={classes.post_image} /> : <img src={x.thumbnail_url} className={classes.post_image}></img>}
                                    
                                </Avatar>
                                <div className={classes.post_details}>
                                    <Link href={"/post/" + x._id}>
                                        <p className={classes.post_title}>{x.title}</p>
                                    </Link>
                                    <div className={classes.post_action}>
                                        <Link href={"/post/edit/" + x._id}>
                                            <p>Edit</p>
                                        </Link>
                                        <Link href={"/post/delete/" + x._id}>
                                            <p>Delete</p>
                                        </Link>
                                    </div>
                                </div>

                            </div>
                        )
                    })
                )
            }

            else {
                
                return (
                    <div className={classes.no_posts}>
                        <h2>You don't have any posts!</h2>
                        <Link href="/post/new">New Post</Link>
                    </div>
                )
            }

        }



        return (
            <Layout isLoggedIn={this.props.isLoggedIn}>
                
                <MessageBar 
                    errorMessages = {this.state.error_messages}
                    infoMessages = {this.state.info_messages}
                    successMessages = {this.state.success_messages}
                />

                   

                <div className={classes.root}>
                    <div className={classes.user}>
                        <h1>Welcome {this.state.email}</h1>
                    </div>
                    <div className={classes.postlist}>

                        <Posts />

                    </div>
                </div>
            </Layout>
        )

    }
}

function getAuthHeader(cookie) {
    cookie = cookie.replace("access_token=", "");
    return {
        Authorization: 'Bearer ' + cookie,
        'Access-Control-Allow_Origin': '*'
    }
}

Profile.getInitialProps = async function (context) {

    console.log(context)
    const access_token = auth(context)




    console.log('what')
    console.log(!auth(access_token));

    if (!access_token) {
        customRedirect(context, '/login')
        return {}
    }



    const res_profile = await fetch(URL.GET_PROFILE, {
        headers: {
            Authorization: 'Bearer ' + access_token,
            'Access-Control-Allow_Origin': '*'
        }
    })



    const data = await res_profile.json()

    console.log(URL.GET_USER_POSTS + "/" + data.email)
    const res_posts = await fetch(URL.GET_USER_POSTS + "/" + data.email)

    const posts_data = await res_posts.json()
    console.log('que')
    return {
        profile: { data, posts_data },
        

    }
}


export default withAuthSync(withStyles(styles)(Profile));