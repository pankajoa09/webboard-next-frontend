import React from 'react'
import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';
import * as URL from '../utils/urls';



export default function MyEditor(props) {
    console.log(props)
    return(
    <Editor
        //disabled={this.state.isLoading}
        disabled={props.disabled}
        iditialValue="<p>this is the initial content of the editor </p>"
        //value={this.state.detail}
        value={props.value}
        apiKey="f6of9mnronvokajb00k69wag6uutos5ng7m75gewlajhlh1w"
        init={{
            height: 200,
            menubar: false,
            selector: 'textarea',
            images_upload_handler: function (blobInfo, success, failure) {
                setTimeout(function () {
                    var form = new FormData();
                    form.append('File', blobInfo.blob(), blobInfo.blob().name);
                    let data = new FormData();
                    data.append('name', 'image')
                    data.append('file', blobInfo.blob())
                    let config = {
                        header: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                    axios.post(URL.UPLOAD_PHOTO, data, config).then(response => {
                        console.log('response', response)
                        success(response.data)

                    }).catch(error => {
                        console.log('error', error)
                    })
                });

            },
            plugins: [
                'advlist autolink lists link image imagetools charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar:
                'undo redo | formatselect | bold italic backcolor | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | image code\
                '
        }}
        //onEditorChange={this.handleEditorChange}
        onEditorChange={props.onEditorChange}
        textareaName="something"
        

    />
    )

}