import Link from 'next/link';
import Layout from '../components/layout'
import fetch from 'isomorphic-unfetch';
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Container } from '@material-ui/core';
import { withAuthSync } from '../utils/auth'
import * as URL from '../utils/urls';
import MessageBar from './messagebar';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    root: {
        '& a': {
            color: 'white'
        }
    }
});

function DisplayPosts(params) {
    const { posts } = params;
    return (
        <Grid container>
            {posts.map((post, i) => (
                //  <Link href={"/post/" + post._id}><a title="Post Page"><div>{post.title}</div></a></Link>
                //  <Link href={"/post/[id]"} as={"post/" + post._id}><a title="Post Page"><div>{post.title}</div></a></Link>

                <Grid item xs={12} md={3} key={'Post_' + i} >
                    <Link href={"/post/[id]"} as={"post/" + post._id} key={'post_' + i}>
                        <Card>
                            <CardActionArea>
                                <CardMedia
                                    image="https://via.placeholder.com/150"
                                    title="Post Picture Placeholder"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {post.title}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Link>
                </Grid>

                // <div key={'post_'+i}>
                //     <Link href={"/post/[id]"} as={"post/" + post._id}>
                //         <a title="Post Page">
                //             <div>{post.title}</div>
                //         </a>
                //     </Link>
                // </div>
            ))}
        </Grid>
    )
}


class Index extends React.Component {

    constructor(props) {
        super(props)
        this._ref = props._ref
        this.state = {
            error_messages: [],
            success_messages: [],
            info_messages: []
        }
    }

    componentDidMount() {
        if (!this.props.isLoggedIn) {
            //this.setState({info_messages: ['Log In?']})
            this.setState({ info_messages: [<Link href="/login">Log In?</Link>] })
        }
    }

    render() {
        const { classes } = this.props;
        return (
            <Layout isLoggedIn={this.props.isLoggedIn}>

                <div className={classes.root}>
                    <MessageBar
                        errorMessages={this.state.error_messages}
                        infoMessages={this.state.info_messages}
                        successMessages={this.state.success_messages}
                    />
                </div>
                <Grid container alignContent="center">
                    <Container>
                        <Typography variant="h6">
                            Home Page
                    </Typography>
                        <DisplayPosts posts={this.props.posts} />
                    </Container>
                </Grid>
            </Layout>
        )

    }

}

Index.getInitialProps = async function (context) {
    const res = await fetch(URL.GET_ALL_POSTS);
    const data = await res.json();

    return { posts: data };
}

export default withAuthSync(withStyles(styles)(Index));