import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

import Router from 'next/router';

import { login } from '../utils/auth'
import { withAuthSync } from '../utils/auth'
import * as URL from '../utils/urls';
import Layout from '../components/layout';

import MessageBar from './messagebar'

import IndeterminateProgress from './indeterminateprogress';






const styles = theme => ({
    form: {
        marginTop: 40,
        margin: 20,
    },
    title: {
        textAlign: 'center',
        color: '#A10020',
        fontSize: 49,
        fontWeight: 500
    },
    email: {
        margin: 17,
        display: 'block',
        width: "100%",

    },
    password: {
        margin: 17,
        display: 'block',
        width: "100%",
    },

    button: {
        marginTop: 40,
        display: 'block',
        width: '100%',
        margin: 17,
        borderRadius: 0,
        backgroundColor: 'white',
        height: 54,
        borderWidth: 1,
        borderColor: 'grey',
        borderStyle: 'solid',
        color: 'grey',
    },
    formWrapper: {
        '& .MuiButton-root:hover': {
            backgroundColor: '#A10020',
            color: 'white',
            borderColor: 'white',
        },
        '& label.Mui-focused': {
            color: '#A10020',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: '#A10020',
        },
        '& .MuiOutlinedInput-root': {
            borderRadius: 0,
            '&.Mui-focused fieldset': {
                borderColor: '#A10020',
            },
        }
    }
});



class LoginPage extends React.Component {

    constructor(props) {
        super(props)
        this._ref = props._ref
        this.state = {
            email: '',
            password: '',
            isLoading: false,
            textFieldEmpty: false,
            passwordFieldEmpty: false,            
            error_messages: [],
            success_messages: [],
            info_messages: []

        }

    }

    componentDidMount() {
        this.authCheck(this.props.isLoggedIn);
        this.justLoggedOut(this.props.isLoggedIn, this.props.history)
    }



    authCheck = (isLoggedIn) => {
        
        if (isLoggedIn) {
            Router.push('/profile')
        }
        return '';
    }

    justLoggedOut = (isLoggedIn, history) => {

        const wasLoggingOut = history[history.length - 1] === '/logout'

        const hasBeenInWebsite = (history.length !== 0) && (history[history.length] !== '/login')


        
        
        if (wasLoggingOut && !isLoggedIn) {
            
            this.setState({ success_messages: ['Logged Out!'] })
            setTimeout(() => this.setState({ success_messages: [] }), 3000)
            


        }
        else if (!wasLoggingOut && !isLoggedIn && hasBeenInWebsite) {
            
            this.setState({ info_messages: ['Session Expired'] })
            setTimeout(() => this.setState({ info_message: [] }), 3000)
            
        }
        
    }



    formSubmission = async (e) => {
        e.preventDefault();
    
        if ((this.state.email === '') || (this.state.password === '')) {
            if (this.state.email === ''){
                this.setState({emailFieldEmpty : true})
            }
            if (this.state.password === ''){
                this.setState({passwordFieldEmpty : true})
            }
        }
        else {
            var myData = new FormData(e.target)
            this.setState({ isLoading: true })
            this.setState({ info_messages: ['Authenticating ...']})

            const response = await fetch(URL.LOGIN, {
                method: 'post',
                body: myData
            }).then(res => {
                return Promise.all([res.status, res.json()])
            })
                .then(result => {
                    this.setState({ isLoading: false })
                    this.setState({ info_messages: []})
                    const responseCode = result[0]
                    const data = result[1]
                    if (responseCode === 401) {
                        throw data
                    } else if (responseCode === 200) {
                        const { access_token } = data
                        
                        login(access_token)

                        Router.push('/profile')
                    }
                })
                .catch(e => {
                    
                    this.setState({ error_messages: [e.message] })

                    
                    

                })
        }
    }

    handleEmailChange = (e) => {
        if (e.target.value !== ''){
            this.setState({emailFieldEmpty:false})
            this.setState({error_messages: []})
        }
        this.setState({ email: e.target.value })
    }
    handlePasswordChange = (e) => {
        if (e.target.value !== ''){
            this.setState({passwordFieldEmpty:false})
            this.setState({error_messages: []})
        }
        this.setState({ password: e.target.value })
    }



    render() {
        const { classes } = this.props;
        
        
        
        return (

            <Layout isLoggedIn={this.props.isLoggedIn}>

                <MessageBar
                    errorMessages={this.state.error_messages}
                    infoMessages={this.state.info_messages}
                    successMessages={this.state.success_messages}
                />
                <Container>
                    <Grid container>
                        <Grid item xs={4}></Grid>
                        <Grid item xs={4}>
                            <form onSubmit={this.formSubmission} className={classes.form} noValidate>
                                <div className={classes.formWrapper}>

                                    <h1 className={classes.title}>Login</h1>
                                    <TextField name='email'
                                        error={this.state.emailFieldEmpty || (this.state.error_messages.length !== 0)}
                                        onChange={this.handleEmailChange}
                                        id={this.state.emailFieldEmpty === true ? "outlined-error-helper-text" : 'outlined-basic'}
                                        
                                        label="EMAIL"
                                        variant="outlined"
                                        className={classes.email}
                                        helperText={this.state.emailFieldEmpty === true ? 'Username Field Is Empty' : null}
                                        disabled = {this.state.isLoading}
                                        fullWidth />


                                    <TextField
                                        error={this.state.passwordFieldEmpty || (this.state.error_messages.length !== 0)}
                                        name='password'
                                        onChange={this.handlePasswordChange}
                                        id={this.state.passwordFieldEmpty === true ? "outlined-error-helper-text" : 'outlined-basic'}
                                        label="PASSWORD"
                                        type="password"
                                        variant="outlined"
                                        className={classes.password}
                                        helperText={this.state.passwordFieldEmpty === true ? 'Password Field Is Empty' : null}
                                        disabled = {this.state.isLoading}
                                        fullWidth />


                                    <Button type="submit"
                                        className={classes.button}
                                        disabled={this.state.isLoading}>
                                        Login
                                    </Button>

                                    {this.state.isLoading ? <IndeterminateProgress /> : null}

                                    
                                </div>

                            </form>

                        </Grid>

                        <Grid item xs={4}></Grid>
                    </Grid>
                </Container>
            </Layout>
        )
    }
}

LoginPage.getInitialProps = async function (context) {
    return {}
}

export default withAuthSync(withStyles(styles)(LoginPage));