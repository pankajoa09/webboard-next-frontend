export const API = 'http://localhost:5000'

export const GET_IMAGE = API + '/api_get_image'
export const GET_POST = API + '/api_get_post'
export const GET_ALL_POSTS = API + '/api_get_all_posts'
export const GET_PROFILE = API + '/api_get_profile'
export const GET_USER_POSTS = API + '/api_get_user_posts'
export const GET_USER_COMMENTS = API + '/api_get_user_comments'
export const GET_COMMENTS_OF_POST = API + '/api_get_post_comments'
export const GET_ALL_CATEGORIES = API + '/api_get_all_categories'


export const UPLOAD_PHOTO = API + '/api_upload_photo'
export const ADD_POST = API + '/api_add_post'
export const ADD_COMMENT = API + '/api_add_comment'

export const UPDATE_POST = API + '/api_update_post'
export const UPDATE_COMMENT = API + '/api_update_comment'

export const DELETE_POST = API + '/api_delete_post'
export const DELETE_COMMENT = API + '/api_delete_comment'

export const SIGNUP = API + '/api_signup'
export const LOGIN = API + '/api_login'


//import * as URL from '../utils/urls';


