import Router from "next/router"

export const customRedirect = (ctx,path,rCode=301) => {
    if (typeof window === 'undefined'){
        ctx.res.writeHead(rCode,{Location: path})
        ctx.res.end()
        return {}
    }

    Router.replace(path)
    return {}
}