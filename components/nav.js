// import React from 'react';
import Link from '../src/Link'
import Router from 'next/router'
import Head from 'next/head'
import NProgress from 'nprogress'

import { makeStyles, fade } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
// import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import Container from '@material-ui/core/Container'
// import { logout } from '../utils/auth'
// import Nav from '../components/nav'
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
// import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import { withAuthSync } from '../utils/auth'

Router.events.on('routeChangeStart', url => {
    console.log(`Loading: ${url}`)
    NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())



const useStyles = makeStyles(theme => ({
    root: {

        backgroundColor: '#A10020',
        '& a': {
            color: '#FFF',
        }
    },
    container: {
        maxWidth: "80%"
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
        margin: theme.spacing(2),
        '& a': {
            marginLeft: 14,
        }
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    appbar: {
        backgroundColor: '#A10020',
    }
}));


const loggedinMenu = (params) => {

    const isLoggedin = params.isLoggedin;
    console.log("this is in layout", this.access_token)

    if (isLoggedin) {
        return (
            <React.Fragment>
                <Link href="/profile">
                    <Button color="inherit">
                        Profile
                    </Button>
                </Link>

                <Button color="inherit" onClick={this.handleLogoutClick}>
                    Logout
                </Button>
            </React.Fragment>
        )
    }

    return (
        <React.Fragment>
            <Link href="/login">
                <Button color="inherit">
                    Login
              </Button>
            </Link>

            <Link href="/signup">
                <Button color="inherit">
                    Sign Up
              </Button>
            </Link>
        </React.Fragment>
    )
}

function Nav(props) {

    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleProfileMenuOpen = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
        handleMobileMenuClose();
    };

    const handleMobileMenuOpen = event => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const menuId = 'primary-search-account-menu';
    const renderMenu = (

        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
            <MenuItem onClick={handleMenuClose}>My account</MenuItem>
        </Menu>
    );

    const mobileMenuId = 'primary-search-account-menu-mobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem>
                <IconButton aria-label="show 4 new mails" color="inherit">
                    <Badge badgeContent={4} color="secondary">
                        <MailIcon />
                    </Badge>
                </IconButton>
                <p>Messages</p>
            </MenuItem>
            <MenuItem>
                <IconButton aria-label="show 11 new notifications" color="inherit">
                    <Badge badgeContent={11} color="secondary">
                        <NotificationsIcon />
                    </Badge>
                </IconButton>
                <p>Notifications</p>
            </MenuItem>
            <MenuItem onClick={handleProfileMenuOpen}>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="primary-search-account-menu"
                    aria-haspopup="true"
                    color="inherit"
                >
                    <AccountCircle />
                </IconButton>
                <p>Profile</p>
            </MenuItem>
        </Menu>
    );


    const LoggedInMenu = () => {
        return (
            <div>
                <Link href="/post/new">
                    <Typography variant="h6" className={classes.title}>
                        New Post
                </Typography>
                </Link>
                <Link href="/profile">
                    <Typography variant="h6" className={classes.title}>
                        Profile
                </Typography>
                </Link>
                <Link href="/logout">
                    <Typography variant="h6" className={classes.title}>
                        Log Out
                </Typography>
                </Link>
            </div>
        )
    }

    const LoggedOutMenu = () => {
        return (
            <div>
                <Link href="/signup">
                    <Typography variant="h6" className={classes.title}>
                        Sign Up
                </Typography>
                </Link>
                <Link href="/login">
                    <Typography variant="h6" className={classes.title}>
                        Log In
                </Typography>
                </Link>
                <Link href="/about">
                    <Typography variant="h6" className={classes.title}>
                        About
                </Typography>
                </Link>
            </div>
        )
    }

    const AuthorizationMenu = () => {
        console.log("ARE YOU EVEN LISTENINGER")
        if (props.isLoggedin) {
            return (<div>
                <Link href="/post/new">
                    <Typography variant="h6" className={classes.title}>
                        New Post
            </Typography>
                </Link>
                <Link href="/profile">
                    <Typography variant="h6" className={classes.title}>
                        Profile
            </Typography>
                </Link>
                <Link href="/logout">
                    <Typography variant="h6" className={classes.title}>
                        Log Out
            </Typography>
                </Link>
            </div>)

        }
        return (
            <div>
                <Link href="/signup">
                    <Typography variant="h6" className={classes.title}>
                        Sign Up
        </Typography>
                </Link>
                <Link href="/login">
                    <Typography variant="h6" className={classes.title}>
                        Log In
        </Typography>
                </Link>
                <Link href="/about">
                    <Typography variant="h6" className={classes.title}>
                        About
        </Typography>
                </Link>
            </div>)
    }









    return (
        <div className={classes.root}>
            
            <AppBar position="static" className={classes.appbar}>
                <Container className={classes.container}>
                    <Toolbar className={classes.toolbar}>

                        <div className={classes.logo}>
                            <Link href="/">
                                <Typography variant="h6" className={classes.title}>
                                    Chat Up
                </Typography>
                            </Link>
                        </div>
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <InputBase
                                placeholder="Search…"
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                                inputProps={{ 'aria-label': 'search' }}
                            />
                        </div>
                        <div className={classes.grow} />
                        {props.isLoggedIn ?
                            <div className={classes.sectionDesktop}>


                                <Link href="/post/new">
                                    <Typography variant="h6" className={classes.title}>
                                        New Post
                                        </Typography>
                                </Link>
                                <Link href="/profile">
                                    <Typography variant="h6" className={classes.title}>
                                        Profile
                                        </Typography>
                                </Link>
                                <Link href="/logout">
                                    <Typography variant="h6" className={classes.title}>
                                        Log Out
                                        </Typography>
                                </Link>


                            </div>
                            : <div className={classes.sectionDesktop}>
                                <Link href="/signup">
                                    <Typography variant="h6" className={classes.title}>
                                        Sign Up
                            </Typography>
                                </Link>
                                <Link href="/login">
                                    <Typography variant="h6" className={classes.title}>
                                        Log In
                            </Typography>
                                </Link>
                                <Link href="/about">
                                    <Typography variant="h6" className={classes.title}>
                                        About
                            </Typography>
                                </Link>
                            </div>}


                        <div className={classes.sectionMobile}>
                            <IconButton
                                aria-label="show more"
                                aria-controls={mobileMenuId}
                                aria-haspopup="true"
                                onClick={handleMobileMenuOpen}
                                color="inherit"
                            >
                                <MoreIcon />
                            </IconButton>
                        </div>
                    </Toolbar>
                </Container>
            </AppBar>
            {renderMobileMenu}
            {renderMenu}
        </div>
    )
}

export default Nav;




/*
    const UseLater = (
        <div>
            <IconButton aria-label="show 4 new mails" color="inherit">
                <Badge badgeContent={4} color="secondary">
                    <MailIcon />
                </Badge>
            </IconButton>
            <IconButton aria-label="show 11 new notifications" color="inherit">
                <Badge badgeContent={11} color="secondary">
                    <NotificationsIcon />
                </Badge>
            </IconButton>

            <IconButton
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
            >
                <AccountCircle />
            </IconButton>
        </div>
    )
*/