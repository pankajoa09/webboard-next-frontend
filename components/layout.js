import React from 'react';
// import Link from 'next/link'
// import Router from 'next/router'
import { withStyles, fade } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
// import SearchIcon from '@material-ui/icons/Search';
// import Button from '@material-ui/core/Button';
// import Grid from '@material-ui/core/Grid';
// import InputBase from '@material-ui/core/InputBase';

// import { logout } from '../utils/auth'
import Nav from '../components/nav'
import { Container } from '@material-ui/core';
import ProgressBar from '../pages/progressbar'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  children: {
    paddingTop: 40,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  toolbar: {
    backgroundColor: '#A10020',
  },
  sidelinks: {
    //float: 'right'
  },
  logo: {
    //float: 'left'
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
    searchIcon: {
      width: theme.spacing(7),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: 200,
      }
    }
  }
});


class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this)

    this.handleLogoutClick = this.handleLogoutClick.bind(this)
  }

  handleClick(link) {
    this._ref.changePage(link)
  }

  handleLogoutClick() {
    console.log('logout')
    logout()
    this.forceUpdate();
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>

        <Nav isLoggedIn={this.props.isLoggedIn} />

        <Container className={classes.children}>
          {this.props.children}
        </Container>
        
      </div>
    )
  }
}

export default withStyles(styles)(Layout);