webpackHotUpdate("static/development/pages/post/[id].js",{

/***/ "./pages/post/[id].js":
/*!****************************!*\
  !*** ./pages/post/[id].js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/layout */ "./components/layout.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core_Input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Input */ "./node_modules/@material-ui/core/esm/Input/index.js");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _utils_auth__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../utils/auth */ "./utils/auth.js");
/* harmony import */ var _material_ui_core_List__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @material-ui/core/List */ "./node_modules/@material-ui/core/esm/List/index.js");
/* harmony import */ var _material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @material-ui/core/ListItem */ "./node_modules/@material-ui/core/esm/ListItem/index.js");
/* harmony import */ var _material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @material-ui/core/Divider */ "./node_modules/@material-ui/core/esm/Divider/index.js");
/* harmony import */ var _material_ui_core_ListItemText__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @material-ui/core/ListItemText */ "./node_modules/@material-ui/core/esm/ListItemText/index.js");
/* harmony import */ var _material_ui_core_ListItemAvatar__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @material-ui/core/ListItemAvatar */ "./node_modules/@material-ui/core/esm/ListItemAvatar/index.js");
/* harmony import */ var _material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @material-ui/core/Avatar */ "./node_modules/@material-ui/core/esm/Avatar/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
/* harmony import */ var _utils_urls__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../utils/urls */ "./utils/urls.js");
/* harmony import */ var _MyEditor__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../MyEditor */ "./pages/MyEditor.js");
/* harmony import */ var _messagebar__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../messagebar */ "./pages/messagebar.js");
/* harmony import */ var _indeterminateprogress__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../indeterminateprogress */ "./pages/indeterminateprogress.js");








var _jsxFileName = "/Users/pankajahuja/Downloads/hello-next 2/pages/post/[id].js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_14___default.a.createElement;

function _createSuper(Derived) { return function () { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
























var styles = function styles(theme) {
  return {
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: "100%"
      }
    },
    title: {
      color: '#A10020',
      fontSize: 49,
      textAlign: "center",
      fontWeight: 500
    },
    category: {
      textAlign: "center",
      borderStyle: "solid",
      borderWidth: 1,
      borderColor: "lightgrey",
      marginTop: 13,
      marginBotton: 13,
      color: "grey",
      fontWeight: 600,
      fontSize: 22
    },
    detail: {
      fontSize: 22
    },
    editor: {
      borderWidth: 2,
      borderStyle: 'solid',
      borderColor: '#ff1744'
    },
    editorValidationText: {
      color: '#ff1744',
      marginLeft: 15,
      marginTop: 5
    },
    button: {
      marginTop: 40,
      display: 'block',
      width: '100%',
      margin: 17,
      borderRadius: 0,
      backgroundColor: 'white',
      height: 54,
      borderWidth: 1,
      borderColor: 'grey',
      borderStyle: 'solid',
      color: 'grey'
    },
    formWrapper: {
      '& .MuiButton-root:hover': {
        backgroundColor: '#A10020',
        color: 'white',
        borderColor: 'white'
      },
      '& label.Mui-focused': {
        color: '#A10020'
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#A10020'
      },
      '& .MuiOutlinedInput-root': {
        borderRadius: 0,
        '&.Mui-focused fieldset': {
          borderColor: '#A10020'
        }
      }
    }
  };
};

var Post = /*#__PURE__*/function (_React$Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Post, _React$Component);

  var _super = _createSuper(Post);

  function Post(props) {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Post);

    _this = _super.call(this, props);

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "updateComments", function _callee() {
      var res_comments, comment_details;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_26__["GET_COMMENTS_OF_POST"] + '/' + _this.props.post[0]._id));

            case 2:
              res_comments = _context.sent;
              _context.next = 5;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_comments.json());

            case 5:
              comment_details = _context.sent;

              _this.setState({
                comments: comment_details
              });

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, null, null, null, Promise);
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleEditorChange", function (content, editor) {
      if (_this.isValid(content)) {
        _this.setState({
          commentIsInvalid: false
        });

        _this.setState({
          error_messages: []
        });
      }

      _this.setState({
        comment: content
      });
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "isValid", function (text) {
      var cond = text.length > 2;
      return cond;
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleSubmit", function (e) {
      e.preventDefault();
      console.log(_this.state.comment);
      var myData = new FormData(e.target);
      console.log('///////////');
      console.log('///////////');
      console.log('///////////');
      console.log('///////////');
      console.log(_this.props);

      if (!_this.isValid(_this.state.comment)) {
        console.log('yo no tengo');

        _this.setState({
          commentIsInvalid: true
        });
      } else {
        _this.setState({
          commentIsInValid: false
        });

        _this.setState({
          isLoading: true
        });

        _this.setState({
          info_messages: ["Posting Comment ..."]
        });

        isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_26__["ADD_COMMENT"], {
          method: 'post',
          body: myData,
          headers: {
            Authorization: 'Bearer ' + js_cookie__WEBPACK_IMPORTED_MODULE_16___default.a.get('access_token'),
            'Access-Control-Allow_Origin': '*'
          }
        }).then(function (res) {
          return Promise.all([res.json(), res.status]);
        }).then(function (result) {
          var data = result[0];
          var resCode = result[1];
          console.log('Response Code: ' + resCode + " Data: " + data['error']);

          if (resCode === 401 || resCode === 422) {
            next_router__WEBPACK_IMPORTED_MODULE_17___default.a.push('/login');
          } else if (resCode === 400) {
            //this.handleErrorFromServer(data['error'])
            console.log(data);
          } else if (resCode === 200) {
            _this.updateComments();

            _this.setState({
              isLoading: false
            });

            _this.setState({
              info_messages: []
            });

            _this.setState({
              success_messages: ['Successfully Posted Comment!']
            });
          } else {
            next_router__WEBPACK_IMPORTED_MODULE_17___default.a.push('/login');
          }
        });
      }
    });

    _this._ref = props._ref;
    _this.state = {
      comments: _this.props.comments,
      comment: '',
      commentIsInValid: false,
      isLoading: false,
      error_messages: [],
      info_messages: [],
      success_messages: []
    };
    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Post, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var classes = this.props.classes;
      var post = this.props.post[0];

      var Comments = function Comments() {
        return _this2.state.comments.map(function (comment) {
          return __jsx("div", {
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 212,
              columnNumber: 17
            }
          }, __jsx(_material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_20__["default"], {
            alignItems: "flex-start",
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 213,
              columnNumber: 21
            }
          }, __jsx(_material_ui_core_ListItemAvatar__WEBPACK_IMPORTED_MODULE_23__["default"], {
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 214,
              columnNumber: 25
            }
          }, __jsx(_material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_24__["default"], {
            alt: comment.user,
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 215,
              columnNumber: 29
            }
          })), __jsx(_material_ui_core_ListItemText__WEBPACK_IMPORTED_MODULE_22__["default"], {
            primary: comment.user,
            secondary: __jsx(react__WEBPACK_IMPORTED_MODULE_14___default.a.Fragment, {
              __self: _this2,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 220,
                columnNumber: 33
              }
            }, __jsx(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_25__["default"], {
              component: "span",
              variant: "body2",
              className: classes.inline,
              color: "textPrimary",
              __self: _this2,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 221,
                columnNumber: 37
              }
            }), __jsx("div", {
              className: classes.comment,
              dangerouslySetInnerHTML: {
                __html: comment.comment
              },
              __self: _this2,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 231,
                columnNumber: 38
              }
            })),
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 217,
              columnNumber: 25
            }
          })), __jsx(_material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_21__["default"], {
            variant: "inset",
            component: "li",
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 236,
              columnNumber: 21
            }
          }));
        });
      };

      return __jsx(_components_layout__WEBPACK_IMPORTED_MODULE_8__["default"], {
        isLoggedIn: this.props.isLoggedIn,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242,
          columnNumber: 13
        }
      }, __jsx(_messagebar__WEBPACK_IMPORTED_MODULE_28__["default"], {
        errorMessages: this.state.error_messages,
        infoMessages: this.state.info_messages,
        successMessages: this.state.success_messages,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243,
          columnNumber: 17
        }
      }), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        container: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 249,
          columnNumber: 17
        }
      }, __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        item: true,
        xs: 3,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250,
          columnNumber: 21
        }
      }), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        item: true,
        xs: 6,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 252,
          columnNumber: 21
        }
      }, __jsx("div", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253,
          columnNumber: 25
        }
      }, __jsx("div", {
        className: classes.title,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 255,
          columnNumber: 29
        }
      }, post.title), __jsx("div", {
        className: classes.category,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 258,
          columnNumber: 29
        }
      }, post.category_name), __jsx("div", {
        className: classes.detail,
        dangerouslySetInnerHTML: {
          __html: post.detail
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 261,
          columnNumber: 29
        }
      }), this.props.isLoggedIn ? __jsx("div", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 263,
          columnNumber: 33
        }
      }, __jsx("form", {
        className: classes.root,
        onSubmit: this.handleSubmit,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265,
          columnNumber: 37
        }
      }, __jsx("input", {
        name: "comment",
        className: classes.textField,
        value: this.state.comment,
        onChange: this.handleDetailChange,
        type: "hidden",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 266,
          columnNumber: 41
        }
      }), __jsx("div", {
        className: this.state.commentIsInvalid ? classes.editor : null,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269,
          columnNumber: 41
        }
      }, __jsx(_MyEditor__WEBPACK_IMPORTED_MODULE_27__["default"], {
        onEditorChange: this.handleEditorChange,
        value: this.state.comment,
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271,
          columnNumber: 45
        }
      })), this.state.commentIsInvalid ? __jsx("p", {
        className: classes.editorValidationText,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 278,
          columnNumber: 72
        }
      }, "Comment must contain at least one character") : null, this.state.isLoading ? __jsx(_indeterminateprogress__WEBPACK_IMPORTED_MODULE_29__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 279,
          columnNumber: 65
        }
      }) : null, __jsx("input", {
        type: "hidden",
        value: post._id,
        name: "post_id",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 280,
          columnNumber: 41
        }
      }), __jsx("div", {
        className: classes.formWrapper,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 284,
          columnNumber: 41
        }
      }, __jsx(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_11__["default"], {
        className: classes.button,
        type: "submit",
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 285,
          columnNumber: 45
        }
      }, "Comment As ", this.props.profile.email)))) : __jsx(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_11__["default"], {
        className: classes.button,
        type: "submit",
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 296,
          columnNumber: 35
        }
      }, "Comment dicks"), __jsx(_material_ui_core_List__WEBPACK_IMPORTED_MODULE_19__["default"], {
        className: classes.root,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 304,
          columnNumber: 29
        }
      }, __jsx(Comments, {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 305,
          columnNumber: 33
        }
      })))), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        item: true,
        xs: 3,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 309,
          columnNumber: 21
        }
      })));
    }
  }]);

  return Post;
}(react__WEBPACK_IMPORTED_MODULE_14___default.a.Component);

Post.getInitialProps = function _callee2(context) {
  var res_post, post_details, res_comments, comment_details, res_profile, data;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_26__["GET_POST"] + '/' + context.query.id));

        case 2:
          res_post = _context2.sent;
          _context2.next = 5;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_post.json());

        case 5:
          post_details = _context2.sent;
          _context2.next = 8;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_26__["GET_COMMENTS_OF_POST"] + '/' + context.query.id));

        case 8:
          res_comments = _context2.sent;
          _context2.next = 11;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_comments.json());

        case 11:
          comment_details = _context2.sent;
          console.log("--------------");
          console.log(comment_details);
          console.log("--------------");
          _context2.next = 17;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_15___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_26__["GET_PROFILE"], {
            headers: {
              Authorization: 'Bearer ' + Object(_utils_auth__WEBPACK_IMPORTED_MODULE_18__["auth"])(context),
              'Access-Control-Allow_Origin': '*'
            }
          }));

        case 17:
          res_profile = _context2.sent;
          _context2.next = 20;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_profile.json());

        case 20:
          data = _context2.sent;
          return _context2.abrupt("return", {
            //        id: context.query.id,
            post: post_details,
            comments: comment_details,
            profile: data // title: data.post.title,
            // detail: data.post.detail

          });

        case 22:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, null, Promise);
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_utils_auth__WEBPACK_IMPORTED_MODULE_18__["withAuthSync"])(Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_9__["withStyles"])(styles)(Post)));

/***/ }),

/***/ "./utils/auth.js":
/*!***********************!*\
  !*** ./utils/auth.js ***!
  \***********************/
/*! exports provided: login, logout, auth, withAuthSync */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "login", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logout", function() { return logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "auth", function() { return auth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withAuthSync", function() { return withAuthSync; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_cookies__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next-cookies */ "./node_modules/next-cookies/index.js");
/* harmony import */ var next_cookies__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_cookies__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_6__);




var _this = undefined,
    _jsxFileName = "/Users/pankajahuja/Downloads/hello-next 2/utils/auth.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }





var login = function login(access_token) {
  js_cookie__WEBPACK_IMPORTED_MODULE_6___default.a.set('access_token', access_token, {
    expires: 1 / (24 * 60)
  });
};
var logout = function logout() {
  js_cookie__WEBPACK_IMPORTED_MODULE_6___default.a.remove('access_token'); // to support logging out from all windows

  window.localStorage.setItem('logout', Date.now());
  next_router__WEBPACK_IMPORTED_MODULE_4___default.a.push('/login');
};
var auth = function auth(ctx) {
  //const { access_token } = ''
  var _nextCookie = next_cookies__WEBPACK_IMPORTED_MODULE_5___default()(ctx),
      access_token = _nextCookie.access_token; // If there's no token, it means the user is not logged in.


  if (!access_token) {
    //   if (typeof window === 'undefined') {
    //     ctx.res.writeHead(302, { Location: '/login' })
    //     ctx.res.end()
    //   } else {
    //     Router.push('/login')
    //   }
    return false;
  }

  return access_token;
};
var withAuthSync = function withAuthSync(WrappedComponent) {
  var Wrapper = function Wrapper(props) {
    var syncLogout = function syncLogout(event) {
      if (event.key === 'logout') {
        console.log('logged out from storage!');
        next_router__WEBPACK_IMPORTED_MODULE_4___default.a.push('/login');
      }
    };

    Object(react__WEBPACK_IMPORTED_MODULE_3__["useEffect"])(function () {
      window.addEventListener('storage', syncLogout);
      return function () {
        window.removeEventListener('storage', syncLogout);
        window.localStorage.removeItem('logout');
      };
    }, []);
    console.log('logged in', props.isLoggedIn);
    return __jsx(WrappedComponent, Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_2__["default"])({}, props, {
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 12
      }
    }));
  };

  Wrapper.getInitialProps = function _callee(ctx) {
    var access_token, isLoggedIn, componentProps;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            access_token = auth(ctx);
            isLoggedIn = access_token ? true : false;
            console.log('isLoggedIn', isLoggedIn);
            _context.t0 = WrappedComponent.getInitialProps;

            if (!_context.t0) {
              _context.next = 8;
              break;
            }

            _context.next = 7;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(WrappedComponent.getInitialProps(ctx));

          case 7:
            _context.t0 = _context.sent;

          case 8:
            componentProps = _context.t0;
            return _context.abrupt("return", _objectSpread({}, componentProps, {
              isLoggedIn: isLoggedIn
            }));

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, null, Promise);
  };

  return Wrapper;
};

/***/ })

})
//# sourceMappingURL=[id].js.2c35c053336a9b33b875.hot-update.js.map