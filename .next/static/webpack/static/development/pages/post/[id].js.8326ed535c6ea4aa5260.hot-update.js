webpackHotUpdate("static/development/pages/post/[id].js",{

/***/ "./pages/post/[id].js":
/*!****************************!*\
  !*** ./pages/post/[id].js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/layout */ "./components/layout.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core_Input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Input */ "./node_modules/@material-ui/core/esm/Input/index.js");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js");
/* harmony import */ var _material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/lab/Skeleton */ "./node_modules/@material-ui/lab/esm/Skeleton/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _utils_auth__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../utils/auth */ "./utils/auth.js");
/* harmony import */ var _material_ui_core_List__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @material-ui/core/List */ "./node_modules/@material-ui/core/esm/List/index.js");
/* harmony import */ var _material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @material-ui/core/ListItem */ "./node_modules/@material-ui/core/esm/ListItem/index.js");
/* harmony import */ var _material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @material-ui/core/Divider */ "./node_modules/@material-ui/core/esm/Divider/index.js");
/* harmony import */ var _material_ui_core_ListItemText__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @material-ui/core/ListItemText */ "./node_modules/@material-ui/core/esm/ListItemText/index.js");
/* harmony import */ var _material_ui_core_ListItemAvatar__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @material-ui/core/ListItemAvatar */ "./node_modules/@material-ui/core/esm/ListItemAvatar/index.js");
/* harmony import */ var _material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @material-ui/core/Avatar */ "./node_modules/@material-ui/core/esm/Avatar/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
/* harmony import */ var _utils_urls__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../../utils/urls */ "./utils/urls.js");
/* harmony import */ var _MyEditor__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../MyEditor */ "./pages/MyEditor.js");
/* harmony import */ var _messagebar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../messagebar */ "./pages/messagebar.js");
/* harmony import */ var _indeterminateprogress__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../indeterminateprogress */ "./pages/indeterminateprogress.js");








var _jsxFileName = "/Users/pankajahuja/Downloads/hello-next 2/pages/post/[id].js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_15___default.a.createElement;

function _createSuper(Derived) { return function () { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

























var styles = function styles(theme) {
  return {
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: "100%"
      }
    },
    avatar: {
      display: 'inline-block'
    },
    username: {
      display: 'inline-block'
    },
    commentText: {},
    title: {
      color: '#A10020',
      fontSize: 49,
      textAlign: "center",
      fontWeight: 500
    },
    category: {
      textAlign: "center",
      borderStyle: "solid",
      borderWidth: 1,
      borderColor: "lightgrey",
      marginTop: 13,
      marginBotton: 13,
      color: "grey",
      fontWeight: 600,
      fontSize: 22
    },
    detail: {
      fontSize: 22
    },
    editor: {
      borderWidth: 2,
      borderStyle: 'solid',
      borderColor: '#ff1744'
    },
    editorValidationText: {
      color: '#ff1744',
      marginLeft: 15,
      marginTop: 5
    },
    button: {
      marginTop: 40,
      display: 'block',
      width: '100%',
      margin: 17,
      borderRadius: 0,
      backgroundColor: 'white',
      height: 54,
      borderWidth: 1,
      borderColor: 'grey',
      borderStyle: 'solid',
      color: 'grey'
    },
    formWrapper: {
      '& .MuiButton-root:hover': {
        backgroundColor: '#A10020',
        color: 'white',
        borderColor: 'white'
      },
      '& label.Mui-focused': {
        color: '#A10020'
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#A10020'
      },
      '& .MuiOutlinedInput-root': {
        borderRadius: 0,
        '&.Mui-focused fieldset': {
          borderColor: '#A10020'
        }
      }
    }
  };
};

var Post = /*#__PURE__*/function (_React$Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Post, _React$Component);

  var _super = _createSuper(Post);

  function Post(props) {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Post);

    _this = _super.call(this, props);

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "updateComments", function _callee() {
      var res_comments, comment_details;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this.setState({
                loadingComments: true
              });

              _context.next = 3;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_27__["GET_COMMENTS_OF_POST"] + '/' + _this.props.post[0]._id));

            case 3:
              res_comments = _context.sent;
              _context.next = 6;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_comments.json());

            case 6:
              comment_details = _context.sent;

              _this.setState({
                comments: comment_details,
                loadingComments: false
              });

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, null, null, null, Promise);
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleEditorChange", function (content, editor) {
      if (_this.isValid(content)) {
        _this.setState({
          commentIsInvalid: false
        });

        _this.setState({
          error_messages: []
        });
      }

      _this.setState({
        comment: content
      });
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "isValid", function (text) {
      var cond = text.length > 2;
      return cond;
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleSubmit", function (e) {
      e.preventDefault();
      console.log(_this.state.comment);
      var myData = new FormData(e.target);
      console.log('///////////');
      console.log('///////////');
      console.log('///////////');
      console.log('///////////');
      console.log(_this.props);

      if (!_this.isValid(_this.state.comment)) {
        console.log('yo no tengo');

        _this.setState({
          commentIsInvalid: true
        });
      } else {
        _this.setState({
          commentIsInValid: false
        });

        _this.setState({
          isLoading: true
        });

        _this.setState({
          info_messages: ["Posting Comment ..."]
        });

        isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_27__["ADD_COMMENT"], {
          method: 'post',
          body: myData,
          headers: {
            Authorization: 'Bearer ' + js_cookie__WEBPACK_IMPORTED_MODULE_17___default.a.get('access_token'),
            'Access-Control-Allow_Origin': '*'
          }
        }).then(function (res) {
          return Promise.all([res.json(), res.status]);
        }).then(function (result) {
          var data = result[0];
          var resCode = result[1];
          console.log('Response Code: ' + resCode + " Data: " + data['error']);

          if (resCode === 401 || resCode === 422) {
            next_router__WEBPACK_IMPORTED_MODULE_18___default.a.push('/login');
          } else if (resCode === 400) {
            //this.handleErrorFromServer(data['error'])
            console.log(data);
          } else if (resCode === 200) {
            _this.updateComments();

            _this.setState({
              comment: ''
            });

            _this.setState({
              isLoading: false
            });

            _this.setState({
              info_messages: []
            });

            _this.setState({
              success_messages: ['Successfully Posted Comment!']
            });

            setTimeout(function () {
              return _this.setState({
                success_messages: []
              });
            }, 2000);
          } else {
            next_router__WEBPACK_IMPORTED_MODULE_18___default.a.push('/login');
          }
        });
      }
    });

    _this._ref = props._ref;
    _this.state = {
      comments: _this.props.comments,
      comment: '',
      commentIsInValid: false,
      isLoading: false,
      error_messages: [],
      info_messages: [],
      success_messages: [],
      loadingComments: false
    };
    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Post, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var classes = this.props.classes;
      var post = this.props.post[0];

      var CommentsDICK = function CommentsDICK() {
        return _this2.state.comments.map(function (comment) {
          return __jsx("div", {
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 229,
              columnNumber: 17
            }
          }, __jsx(_material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_21__["default"], {
            alignItems: "flex-start",
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 230,
              columnNumber: 21
            }
          }, __jsx(_material_ui_core_ListItemAvatar__WEBPACK_IMPORTED_MODULE_24__["default"], {
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 231,
              columnNumber: 25
            }
          }, __jsx(_material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_25__["default"], {
            alt: comment.user,
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 232,
              columnNumber: 29
            }
          })), __jsx(_material_ui_core_ListItemText__WEBPACK_IMPORTED_MODULE_23__["default"], {
            primary: comment.user,
            secondary: __jsx(react__WEBPACK_IMPORTED_MODULE_15___default.a.Fragment, {
              __self: _this2,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 237,
                columnNumber: 33
              }
            }, __jsx("div", {
              className: classes.comment,
              dangerouslySetInnerHTML: {
                __html: comment.comment
              },
              __self: _this2,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 239,
                columnNumber: 38
              }
            })),
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 234,
              columnNumber: 25
            }
          })), __jsx(_material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_22__["default"], {
            variant: "inset",
            component: "li",
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 244,
              columnNumber: 21
            }
          }));
        });
      };

      var Comments = function Comments() {
        return _this2.state.comments.map(function (comment) {
          return __jsx("div", {
            className: classes.comment,
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 253,
              columnNumber: 17
            }
          }, __jsx(_material_ui_core_ListItemAvatar__WEBPACK_IMPORTED_MODULE_24__["default"], {
            className: classes.avatar,
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 254,
              columnNumber: 21
            }
          }, __jsx(_material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_25__["default"], {
            alt: comment.user,
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 255,
              columnNumber: 25
            }
          })), __jsx("div", {
            className: classes.username,
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 257,
              columnNumber: 21
            }
          }, comment.user), __jsx("div", {
            className: classes.commentText,
            dangerouslySetInnerHTML: {
              __html: comment.comment
            },
            __self: _this2,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 258,
              columnNumber: 21
            }
          }));
        });
      };

      return __jsx(_components_layout__WEBPACK_IMPORTED_MODULE_8__["default"], {
        isLoggedIn: this.props.isLoggedIn,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 268,
          columnNumber: 13
        }
      }, __jsx(_messagebar__WEBPACK_IMPORTED_MODULE_29__["default"], {
        errorMessages: this.state.error_messages,
        infoMessages: this.state.info_messages,
        successMessages: this.state.success_messages,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269,
          columnNumber: 17
        }
      }), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        container: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 275,
          columnNumber: 17
        }
      }, __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        item: true,
        xs: 3,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 276,
          columnNumber: 21
        }
      }), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        item: true,
        xs: 6,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 278,
          columnNumber: 21
        }
      }, __jsx("div", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 279,
          columnNumber: 25
        }
      }, __jsx("div", {
        className: classes.title,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281,
          columnNumber: 29
        }
      }, post.title), __jsx("div", {
        className: classes.category,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 284,
          columnNumber: 29
        }
      }, post.category_name), __jsx("div", {
        className: classes.detail,
        dangerouslySetInnerHTML: {
          __html: post.detail
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 287,
          columnNumber: 29
        }
      }), this.props.isLoggedIn ? __jsx("div", {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 289,
          columnNumber: 33
        }
      }, __jsx("form", {
        className: classes.root,
        onSubmit: this.handleSubmit,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 291,
          columnNumber: 37
        }
      }, __jsx("input", {
        name: "comment",
        className: classes.textField,
        value: this.state.comment,
        onChange: this.handleDetailChange,
        type: "hidden",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 292,
          columnNumber: 41
        }
      }), __jsx("div", {
        className: this.state.commentIsInvalid ? classes.editor : null,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295,
          columnNumber: 41
        }
      }, __jsx(_MyEditor__WEBPACK_IMPORTED_MODULE_28__["default"], {
        onEditorChange: this.handleEditorChange,
        value: this.state.comment,
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 297,
          columnNumber: 45
        }
      })), this.state.commentIsInvalid ? __jsx("p", {
        className: classes.editorValidationText,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 304,
          columnNumber: 72
        }
      }, "Comment must contain at least one character") : null, this.state.isLoading ? __jsx(_indeterminateprogress__WEBPACK_IMPORTED_MODULE_30__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 305,
          columnNumber: 65
        }
      }) : null, __jsx("input", {
        type: "hidden",
        value: post._id,
        name: "post_id",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 306,
          columnNumber: 41
        }
      }), __jsx("div", {
        className: classes.formWrapper,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 310,
          columnNumber: 41
        }
      }, __jsx(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_11__["default"], {
        className: classes.button,
        type: "submit",
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311,
          columnNumber: 45
        }
      }, "Comment As ", this.props.profile.email)))) : __jsx("div", {
        className: classes.formWrapper,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 323,
          columnNumber: 33
        }
      }, __jsx(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_11__["default"], {
        className: classes.button,
        onClick: function onClick() {
          return next_router__WEBPACK_IMPORTED_MODULE_18___default.a.push('/login');
        },
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 324,
          columnNumber: 37
        }
      }, "Log In To Comment")), __jsx(_material_ui_core_List__WEBPACK_IMPORTED_MODULE_20__["default"], {
        className: classes.root,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 333,
          columnNumber: 29
        }
      }, this.state.loadingComments ? __jsx(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_14__["default"], {
        animation: "wave",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 334,
          columnNumber: 63
        }
      }) : null, __jsx(Comments, {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 335,
          columnNumber: 33
        }
      })))), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_13__["default"], {
        item: true,
        xs: 3,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 340,
          columnNumber: 21
        }
      })));
    }
  }]);

  return Post;
}(react__WEBPACK_IMPORTED_MODULE_15___default.a.Component);

Post.getInitialProps = function _callee2(context) {
  var res_post, post_details, res_comments, comment_details, res_profile, data;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_27__["GET_POST"] + '/' + context.query.id));

        case 2:
          res_post = _context2.sent;
          _context2.next = 5;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_post.json());

        case 5:
          post_details = _context2.sent;
          _context2.next = 8;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_27__["GET_COMMENTS_OF_POST"] + '/' + context.query.id));

        case 8:
          res_comments = _context2.sent;
          _context2.next = 11;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_comments.json());

        case 11:
          comment_details = _context2.sent;
          console.log("--------------");
          console.log(comment_details);
          console.log("--------------");
          _context2.next = 17;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_16___default()(_utils_urls__WEBPACK_IMPORTED_MODULE_27__["GET_PROFILE"], {
            headers: {
              Authorization: 'Bearer ' + Object(_utils_auth__WEBPACK_IMPORTED_MODULE_19__["auth"])(context),
              'Access-Control-Allow_Origin': '*'
            }
          }));

        case 17:
          res_profile = _context2.sent;
          _context2.next = 20;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res_profile.json());

        case 20:
          data = _context2.sent;
          return _context2.abrupt("return", {
            //        id: context.query.id,
            post: post_details,
            comments: comment_details,
            profile: data // title: data.post.title,
            // detail: data.post.detail

          });

        case 22:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, null, Promise);
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_utils_auth__WEBPACK_IMPORTED_MODULE_19__["withAuthSync"])(Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_9__["withStyles"])(styles)(Post)));

/***/ })

})
//# sourceMappingURL=[id].js.8326ed535c6ea4aa5260.hot-update.js.map