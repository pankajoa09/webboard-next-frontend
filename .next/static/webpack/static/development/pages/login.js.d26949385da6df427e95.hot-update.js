webpackHotUpdate("static/development/pages/login.js",{

/***/ "./pages/login.js":
/*!************************!*\
  !*** ./pages/login.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core_Container__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Container */ "./node_modules/@material-ui/core/esm/Container/index.js");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _utils_auth__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../utils/auth */ "./utils/auth.js");
/* harmony import */ var _utils_urls__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../utils/urls */ "./utils/urls.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../components/layout */ "./components/layout.js");
/* harmony import */ var _messagebar__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./messagebar */ "./pages/messagebar.js");
/* harmony import */ var _indeterminateprogress__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./indeterminateprogress */ "./pages/indeterminateprogress.js");








var _jsxFileName = "/Users/pankajahuja/Downloads/hello-next 2/pages/login.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;

function _createSuper(Derived) { return function () { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }















var styles = function styles(theme) {
  return {
    form: {
      marginTop: 40,
      margin: 20
    },
    title: {
      textAlign: 'center',
      color: '#A10020',
      fontSize: 49,
      fontWeight: 500
    },
    email: {
      margin: 17,
      display: 'block',
      width: "100%"
    },
    password: {
      margin: 17,
      display: 'block',
      width: "100%"
    },
    button: {
      marginTop: 40,
      display: 'block',
      width: '100%',
      margin: 17,
      borderRadius: 0,
      backgroundColor: 'white',
      height: 54,
      borderWidth: 1,
      borderColor: 'grey',
      borderStyle: 'solid',
      color: 'grey'
    },
    formWrapper: {
      '& .MuiButton-root:hover': {
        backgroundColor: '#A10020',
        color: 'white',
        borderColor: 'white'
      },
      '& label.Mui-focused': {
        color: '#A10020'
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#A10020'
      },
      '& .MuiOutlinedInput-root': {
        borderRadius: 0,
        '&.Mui-focused fieldset': {
          borderColor: '#A10020'
        }
      }
    }
  };
};

var LoginPage = /*#__PURE__*/function (_React$Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(LoginPage, _React$Component);

  var _super = _createSuper(LoginPage);

  function LoginPage(props) {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, LoginPage);

    _this = _super.call(this, props);

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "authCheck", function (isLoggedIn) {
      if (isLoggedIn) {
        next_router__WEBPACK_IMPORTED_MODULE_14___default.a.push('/profile');
      }

      return '';
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "justLoggedOut", function (isLoggedIn, history) {
      var wasLoggingOut = history[history.length - 1] === '/logout';
      var hasBeenInWebsite = history.length !== 0 && history[history.length] !== '/login';

      if (wasLoggingOut && !isLoggedIn) {
        _this.setState({
          success_messages: ['Logged Out!']
        });

        setTimeout(function () {
          return _this.setState({
            success_messages: []
          });
        }, 3000);
      } else if (!wasLoggingOut && !isLoggedIn && hasBeenInWebsite) {
        _this.setState({
          info_messages: ['Session Expired']
        });

        setTimeout(function () {
          return _this.setState({
            info_message: []
          });
        }, 3000);
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "formSubmission", function _callee(e) {
      var myData, response;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();

              if (!(_this.state.email === '' || _this.state.password === '')) {
                _context.next = 6;
                break;
              }

              if (_this.state.email === '') {
                _this.setState({
                  emailFieldEmpty: true
                });
              }

              if (_this.state.password === '') {
                _this.setState({
                  passwordFieldEmpty: true
                });
              }

              _context.next = 12;
              break;

            case 6:
              myData = new FormData(e.target);

              _this.setState({
                isLoading: true
              });

              _this.setState({
                info_messages: ['Authenticating ...']
              });

              _context.next = 11;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(fetch(_utils_urls__WEBPACK_IMPORTED_MODULE_16__["LOGIN"], {
                method: 'post',
                body: myData
              }).then(function (res) {
                return Promise.all([res.status, res.json()]);
              }).then(function (result) {
                _this.setState({
                  isLoading: false
                });

                _this.setState({
                  info_messages: []
                });

                var responseCode = result[0];
                var data = result[1];

                if (responseCode === 401) {
                  throw data;
                } else if (responseCode === 200) {
                  var access_token = data.access_token;
                  Object(_utils_auth__WEBPACK_IMPORTED_MODULE_15__["login"])(access_token);
                  next_router__WEBPACK_IMPORTED_MODULE_14___default.a.push('/profile');
                }
              })["catch"](function (e) {
                _this.setState({
                  error_messages: [e.message]
                });
              }));

            case 11:
              response = _context.sent;

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, null, null, null, Promise);
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleEmailChange", function (e) {
      if (e.target.value !== '') {
        _this.setState({
          emailFieldEmpty: false
        });

        _this.setState({
          error_messages: []
        });
      }

      _this.setState({
        email: e.target.value
      });
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handlePasswordChange", function (e) {
      if (e.target.value !== '') {
        _this.setState({
          passwordFieldEmpty: false
        });

        _this.setState({
          error_messages: []
        });
      }

      _this.setState({
        password: e.target.value
      });
    });

    _this._ref = props._ref;
    _this.state = {
      email: '',
      password: '',
      isLoading: false,
      textFieldEmpty: false,
      passwordFieldEmpty: false,
      error_messages: [],
      success_messages: [],
      info_messages: []
    };
    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(LoginPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.authCheck(this.props.isLoggedIn);
      this.justLoggedOut(this.props.isLoggedIn, this.props.history);
    }
  }, {
    key: "render",
    value: function render() {
      var classes = this.props.classes;
      return __jsx(_components_layout__WEBPACK_IMPORTED_MODULE_17__["default"], {
        isLoggedIn: this.props.isLoggedIn,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 217,
          columnNumber: 13
        }
      }, __jsx(_messagebar__WEBPACK_IMPORTED_MODULE_18__["default"], {
        errorMessages: this.state.error_messages,
        infoMessages: this.state.info_messages,
        successMessages: this.state.success_messages,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 219,
          columnNumber: 17
        }
      }), __jsx(_material_ui_core_Container__WEBPACK_IMPORTED_MODULE_11__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 224,
          columnNumber: 17
        }
      }, __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_12__["default"], {
        container: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 225,
          columnNumber: 21
        }
      }, __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_12__["default"], {
        item: true,
        xs: 4,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 226,
          columnNumber: 25
        }
      }), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_12__["default"], {
        item: true,
        xs: 4,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 227,
          columnNumber: 25
        }
      }, __jsx("form", {
        onSubmit: this.formSubmission,
        className: classes.form,
        noValidate: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 228,
          columnNumber: 29
        }
      }, __jsx("div", {
        className: classes.formWrapper,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 229,
          columnNumber: 33
        }
      }, __jsx("h1", {
        className: classes.title,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231,
          columnNumber: 37
        }
      }, "Login"), __jsx(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_9__["default"], {
        name: "email",
        error: this.state.emailFieldEmpty || this.state.error_messages.length !== 0,
        onChange: this.handleEmailChange,
        id: this.state.emailFieldEmpty === true ? "outlined-error-helper-text" : 'outlined-basic',
        label: "EMAIL",
        variant: "outlined",
        className: classes.email,
        helperText: this.state.emailFieldEmpty === true ? 'Username Field Is Empty' : null,
        disabled: this.state.isLoading,
        fullWidth: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 232,
          columnNumber: 37
        }
      }), __jsx(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_9__["default"], {
        error: this.state.passwordFieldEmpty || this.state.error_messages.length !== 0,
        name: "password",
        onChange: this.handlePasswordChange,
        id: this.state.passwordFieldEmpty === true ? "outlined-error-helper-text" : 'outlined-basic',
        label: "PASSWORD",
        type: "password",
        variant: "outlined",
        className: classes.password,
        helperText: this.state.passwordFieldEmpty === true ? 'Password Field Is Empty' : null,
        disabled: this.state.isLoading,
        fullWidth: true,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 245,
          columnNumber: 37
        }
      }), __jsx(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_10__["default"], {
        type: "submit",
        className: classes.button,
        disabled: this.state.isLoading,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 259,
          columnNumber: 37
        }
      }, "Login"), this.state.isLoading ? __jsx(_indeterminateprogress__WEBPACK_IMPORTED_MODULE_19__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265,
          columnNumber: 61
        }
      }) : null))), __jsx(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_12__["default"], {
        item: true,
        xs: 4,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 274,
          columnNumber: 25
        }
      }))));
    }
  }]);

  return LoginPage;
}(react__WEBPACK_IMPORTED_MODULE_8___default.a.Component);

LoginPage.getInitialProps = function _callee2(context) {
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          return _context2.abrupt("return", {});

        case 1:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, null, Promise);
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_utils_auth__WEBPACK_IMPORTED_MODULE_15__["withAuthSync"])(Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_13__["withStyles"])(styles)(LoginPage)));

/***/ })

})
//# sourceMappingURL=login.js.d26949385da6df427e95.hot-update.js.map