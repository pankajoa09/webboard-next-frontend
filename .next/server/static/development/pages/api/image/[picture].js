module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/api/image/[picture].js":
/*!**************************************!*\
  !*** ./pages/api/image/[picture].js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_urls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../utils/urls */ "./utils/urls.js");



/* harmony default export */ __webpack_exports__["default"] = (async (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  const image = req.url.substring(req.url.lastIndexOf('/') + 1, req.url.length);
  console.log(image);
  const server = _utils_urls__WEBPACK_IMPORTED_MODULE_2__["GET_IMAGE"];
  const img_url = server + '/' + image;

  function getBase64(url) {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url, {
      responseType: 'arraybuffer'
    }).then(response => Buffer.from(response.data, 'binary'));
  }

  let image64 = await getBase64(img_url);
  console.log('fuck');
  console.log('ass');
  res.writeHead(200, {
    'Content-Type': 'image/png',
    'Content-Length': image64.length
  });
  res.end(image64);
});

/***/ }),

/***/ "./utils/urls.js":
/*!***********************!*\
  !*** ./utils/urls.js ***!
  \***********************/
/*! exports provided: API, GET_IMAGE, GET_POST, GET_ALL_POSTS, GET_PROFILE, GET_USER_POSTS, GET_USER_COMMENTS, GET_COMMENTS_OF_POST, GET_ALL_CATEGORIES, UPLOAD_PHOTO, ADD_POST, ADD_COMMENT, UPDATE_POST, UPDATE_COMMENT, DELETE_POST, DELETE_COMMENT, SIGNUP, LOGIN */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API", function() { return API; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_IMAGE", function() { return GET_IMAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_POST", function() { return GET_POST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_ALL_POSTS", function() { return GET_ALL_POSTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_PROFILE", function() { return GET_PROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_USER_POSTS", function() { return GET_USER_POSTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_USER_COMMENTS", function() { return GET_USER_COMMENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_COMMENTS_OF_POST", function() { return GET_COMMENTS_OF_POST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_ALL_CATEGORIES", function() { return GET_ALL_CATEGORIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPLOAD_PHOTO", function() { return UPLOAD_PHOTO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_POST", function() { return ADD_POST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_COMMENT", function() { return ADD_COMMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_POST", function() { return UPDATE_POST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_COMMENT", function() { return UPDATE_COMMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_POST", function() { return DELETE_POST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_COMMENT", function() { return DELETE_COMMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SIGNUP", function() { return SIGNUP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGIN", function() { return LOGIN; });
const API = 'http://localhost:5000';
const GET_IMAGE = API + '/api_get_image';
const GET_POST = API + '/api_get_post';
const GET_ALL_POSTS = API + '/api_get_all_posts';
const GET_PROFILE = API + '/api_get_profile';
const GET_USER_POSTS = API + '/api_get_user_posts';
const GET_USER_COMMENTS = API + '/api_get_user_comments';
const GET_COMMENTS_OF_POST = API + '/api_get_post_comments';
const GET_ALL_CATEGORIES = API + '/api_get_all_categories';
const UPLOAD_PHOTO = API + '/api_upload_photo';
const ADD_POST = API + '/api_add_post';
const ADD_COMMENT = API + '/api_add_comment';
const UPDATE_POST = API + '/api_update_post';
const UPDATE_COMMENT = API + '/api_update_comment';
const DELETE_POST = API + '/api_delete_post';
const DELETE_COMMENT = API + '/api_delete_comment';
const SIGNUP = API + '/api_signup';
const LOGIN = API + '/api_login'; //import * as URL from '../utils/urls';

/***/ }),

/***/ 7:
/*!********************************************!*\
  !*** multi ./pages/api/image/[picture].js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/pankajahuja/Downloads/hello-next 2/pages/api/image/[picture].js */"./pages/api/image/[picture].js");


/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ })

/******/ });
//# sourceMappingURL=[picture].js.map